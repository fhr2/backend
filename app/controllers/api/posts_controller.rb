class Api::PostsController < ApplicationController

  def index
    all_posts
  end

  def create
    post = Post.create(post_params)
    render json: { errors: "name and value are mandatory fields." }, status: 422 and return if post.errors.present?
    render json: { post: post }.merge(get_averages)
  end

  def all_posts
    per_page = params[:per_page] || 10
    offset = (params[:page] || 0).to_i*per_page.to_i
    posts = Post.order(:created_at => :asc).reverse_order.limit(per_page).offset(offset)
    render json: { posts: posts }.merge(get_averages)
  end

  def posts_average
    render json: get_averages
  end

  def get_averages
    minutes_avg = (Post.created_between(1.hour.ago, Time.now).count/60.0).round(2)
    hourly_avg = (Post.created_between(1.day.ago, Time.now).count/24.0).round(2)
    daily_avg = (Post.created_between(7.days.ago, Time.now).count/7.0).round(2)
    {daily_avg: daily_avg, hourly_avg: hourly_avg, minutes_avg: minutes_avg}
  end

  private

  def post_params
    params.require(:post).permit(:name, :value)
  end
end
