# FHR assignment Backend

### Prerequisites
* Install ruby 2.7.1

* Install postgres

### To run project
run following commands

* `git clone https://gitlab.com/fhr2/backend.git`
* go to project directory and run `bundle install`
* change Database details in `database.yml` with your database `username` and `password`
* `rake db:create`
* `rake db:migrate`
* `rake db:seed`
* `rails s`

#### Voila, you are in.