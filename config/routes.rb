Rails.application.routes.draw do
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
  namespace :api do
    resources :posts, only: [:index, :create] do
      collection do
        get :posts_average
      end
    end
  end
end
