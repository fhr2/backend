# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)
require 'faker'

1000.times do
  time = Faker::Time.between(from: DateTime.now - 7, to: DateTime.now)
  Post.create(name: Faker::Name.name, value: Faker::Book.title, created_at: time, updated_at: time)
end